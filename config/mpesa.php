<?php

return [
    'api_key' => '9e408023-2b4e-4094-9666-73b3570f8857',
    'short_code' => '602952',
    'consumer_key' => '5gI7KGgiTEkc2O4zh264d8kYa5qFme5S',
    'consumer_secret' => 'tXARcxb0H2F5kKjg',
    'validation_url' => 'sabers.caramacs.com',

    /*
    |--------------------------------------------------------------------------
    | Default Account
    |--------------------------------------------------------------------------
    |
    | This is the default account to be used when none is specified.
    */

    'default' => 'staging',

    /*
    |--------------------------------------------------------------------------
    | File Cache Location
    |--------------------------------------------------------------------------
    |
    | When using the Native Cache driver, this will be the relative directory
    | where the cache information will be stored.
    */

    'cache_location' => '../cache',

    /*
    |--------------------------------------------------------------------------
    | Accounts
    |--------------------------------------------------------------------------
    |
    | These are the accounts that can be used with the package. You can configure
    | as many as needed. Two have been setup for you.
    |
    | Sandbox: Determines whether to use the sandbox, Possible values: sandbox | production
    | Initiator: This is the username used to authenticate the transaction request
    | LNMO:
    |    shortcode: The till number
    |    passkey: The passkey for the till number
    |    callback: Endpoint that will be be queried on completion or failure of the transaction.
    |
    */

    'accounts' => [
        'staging' => [
            'sandbox' => true,
            'key' => '5gI7KGgiTEkc2O4zh264d8kYa5qFme5S',
            'secret' => 'tXARcxb0H2F5kKjg',
            'initiator' => 'apiop69',
            'id_validation_callback' => 'http://sabers.caramacs.com/45cdfe?token=9e408023-2b4e-4094-9666-73b3570f8857',
            'lnmo' => [
                'shortcode' => '174379',
                'passkey' => 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919',
                'callback' => 'http://sabers.caramacs.com/64cv98?token=9e408023-2b4e-4094-9666-73b3570f8857',
            ]
        ],

        'paybill_1' => [
            'sandbox' => true,
            'key' => '5gI7KGgiTEkc2O4zh264d8kYa5qFme5S',
            'secret' => 'tXARcxb0H2F5kKjg',
            'initiator' => 'apiop69',
            'id_validation_callback' => 'http://sabers.caramacs.com/45cdfe?token=9e408023-2b4e-4094-9666-73b3570f8857',
            'lnmo' => [
                'shortcode' => '602952',
                'passkey' => ' 	xU6SCGxz',
                'callback' => 'http://sabers.caramacs.com/64cv98?token=9e408023-2b4e-4094-9666-73b3570f8857',
            ]
        ]
    ],
];