<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUuid;

class HouseLocation extends Model
{
    use HasUuid;
    public $incrementing = false;

    protected $fillable = ['name'];

    public function houses() {
        return $this->hasMany('App\House','house_location_id');
    }
}
