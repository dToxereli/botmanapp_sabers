<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUuid;

class Permission extends \Spatie\Permission\Models\Permission
{
    use HasUuid;
    public $incrementing = false;
}
