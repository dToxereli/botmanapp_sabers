<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\House;
use App\User;
use Validator;
use Log;
use App\Utility\Mpesa;

class PayController extends Controller
{
    public function index()
    {
        return view('pay.index');
    }

    public function complete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'house' => ['required','exists:houses,name'],
            'amount' => ['required','numeric'],
            'mode' => ['string']
        ]);
        $validator->sometimes('mpesa-phone', ['required','string','regex:/^(([0-9]{1,3})|0)[0-9]{9}$/'], function ($input) {
            return $input->mode == 'Mpesa';
        });
        $validator->sometimes('airtel-phone', ['required','string','regex:/^(([0-9]{1,3})|0)[0-9]{9}$/'], function ($input) {
            return $input->mode == 'Airtel money';
        });
        $validator->sometimes('email', ['required','string','email'], function ($input) {
            return $input->mode == 'Paypal';
        });
        $validator->sometimes('cc-name', ['required','string','max:50'], function ($input) {
            return $input->mode == 'Card';
        });
        $validator->sometimes('cc-number', ['required','string','regex:/^([0-9]{4}((-[0-9]){3})$/'], function ($input) {
            return $input->mode == 'Card';
        });
        $validator->sometimes('cc-expiration', ['required','string','regex:/^([0-3][0-9]\/[01][0-9])$/'], function ($input) {
            return $input->mode == 'Card';
        });
        $validator->sometimes('cc-cvv', ['required','string','regex:/^([0-9]{3})$/'], function ($input) {
            return $input->mode == 'Card';
        });
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $house = House::with('user')->where('name',$request['house'])->firstOrFail();
        $input = [
            'amount' => $request['amount'],
            'mode' => $request['mode'],
            'details' => $request->except(['_token','cc-cvv','cc-expiration','cc-name','house','amount','mode'])
        ];
        Log::debug($input);
        $payment = new Payment();
        $payment->fill($input);
        $payment->house_id = $house->id;
        if (isset($house->user)) {
            $payment->user_id = $house->user->id;
        }
        if (array_key_exists('mpesa-phone',$request->all())) {
            $phone = $request['mpesa-phone'];
            // Mpesa::register_urls();
            // Mpesa::stk_push($request['amount'], $phone, $house->name);
            $mpesa= new \Safaricom\Mpesa\Mpesa();
            $stkPushSimulation=$mpesa->STKPushSimulation("174379", "bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919", "CustomerPayBillOnline", $request['amount'], "602952", "600000", $phone, 'https://sabers.caramacs.com/64cv98?token=9e408023-2b4e-4094-9666-73b3570f8857', $house->name, "Payment for Sabers", "Connected Hackathon");

        }
        $payment->save();
        return redirect()->route('pay.thankyou', ['payment' => $payment]);
    }

    public function thankyou()
    {
        return view('pay.thankyou');
    }
}
