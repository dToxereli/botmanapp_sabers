<?php

namespace App\Http\Controllers;

use App\HouseLocation;
use Illuminate\Http\Request;
use Validator;

class HouseLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $houseLocations = HouseLocation::all();
        return view('admin.houselocations.index')->with('houselocations', $houseLocations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.houselocations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required','string','max:150','unique:house_locations']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        HouseLocation::create([
            'name' => $request['name'],
        ]);

        return redirect()->route('admin.houselocations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HouseLocation  $houseLocation
     * @return \Illuminate\Http\Response
     */
    public function show(HouseLocation $houseLocation)
    {
        return view('admin.houselocations.show')->with('houselocation',$houseLocation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HouseLocation  $houseLocation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $houseLocation = HouseLocation::findOrFail($id);
        return view('admin.houselocations.edit')->with('houselocation',$houseLocation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HouseLocation  $houseLocation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $houseLocation = HouseLocation::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => ['required','string','max:150','unique:house_locations,name,'.$houseLocation->id]
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->only(['name']);
        $input = array_filter($input, 'strlen');
        $houseLocation->fill($input)->save();
        return redirect()->route('admin.houselocations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HouseLocation  $houseLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $houseLocation = HouseLocation::findOrFail($id);
        $houseLocation->delete();
        return redirect()->route('admin.houselocations.index');
    }
}
