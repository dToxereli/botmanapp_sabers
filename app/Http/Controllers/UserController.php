<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Permission;
use App\Role;
use App\House;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $owners = User::all();
        return view('admin.owners.index')->with('owners', $owners);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $owner = User::with('houses')->findOrFail($id);
        return view('admin.owners.show')->with('owner', $owner);
    }

    public function addhouse($id)
    {
        $houses = House::all();
    }

    public function sethouse(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $owner = User::findOrFail($id);
        $owner->delete();
        return redirect()->route('admin.owners.index');
    }
}
