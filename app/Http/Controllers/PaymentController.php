<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\House;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::with(['house','user'])->get();
        return view('admin.payments.index')->with('payments', $payments);
    }

    public function show($id)
    {
        $payment = Payment::with(['house','user'])->findOrFail($id);
        return view('admin.payments.show')->with('payment', $payment);
    }

    public function archive(Request $request, $id)
    {
        $payment = Payment::findOrFail($id);
        $payment->archived = true;
        $payment->save();
        return redirect()->route('admin.payments.show',$id);
    }

    public function reverse(Request $request, $id)
    {
        $payment = Payment::findOrFail($id);
        $payment->reversed = true;
        $payment->confirmed = false;
        $payment->save();
        return redirect()->route('admin.payments.show',$id);
    }

    public function confirm(Request $request, $id)
    {
        $payment = Payment::findOrFail($id);
        $payment->confirmed = true;
        $payment->save();
        return redirect()->route('admin.payments.show',$id);
    }

    public function transfer(Request $request, $id)
    {
        $payment = Payment::with('user')->findOrFail($id);
        $validator = Validator::make($request->all(), [
            'house' => ['required','exists:houses,name']
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $house = House::where('name',$request['house'])->where('user_id',$payment->user_id)->first();
        if (!isset($house)) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput()
                    ->with('status','User does not own that house');
        }
        $payment->house_id = $house->id;
        $payment->save();
        return redirect()->route('admin.payments.show',$id);
    }

    
}
