<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Log;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class MPesaController extends Controller
{
    public function con(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => ['required','regex:/^'.config('mpesa.api_key').'$/'],
        ]);        

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        Log::debug($request);
        return response()->json(["ResultCode" => 0, "ResultDesc" => "Confirmation received successfully"], 200);
    }

    public function val(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => ['required','regex:/^'.config('mpesa.api_key').'$/'],
        ]);        

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        Log::debug($request);
        return response()->json(["ResultCode" => 0, "ResultDesc" => "Success", "ThirdPartyTransID" => 0], 200);
    }

    public function reg(Request $request)
    {
        header("Content-Type:application/json");
        $shortcode = config('mpesa.short_code');
        $consumerkey = config('mpesa.consumer_key');
        $consumersecret = config('mpesa.consumer_secret');
        $validationurl = "enteryourvalidationurlhere";
        $confirmationurl = "enteryourconfirmationurlhere";
    }
}
