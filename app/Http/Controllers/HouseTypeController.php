<?php

namespace App\Http\Controllers;

use App\HouseType;
use Illuminate\Http\Request;
use Validator;

class HouseTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $houseTypes = HouseType::all();
        return view('admin.housetypes.index')->with('housetypes', $houseTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.housetypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required','string','max:150','unique:house_types']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        HouseType::create([
            'name' => $request['name'],
        ]);

        return redirect()->route('admin.housetypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HouseType  $houseType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $houseType = HouseType::findOrFail($id);
        return view('admin.housetypes.show')->with('housetype',$houseType);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HouseType  $houseType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $houseType = HouseType::findOrFail($id);
        return view('admin.housetypes.edit')->with('housetype',$houseType);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HouseType  $houseType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $houseType = HouseType::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => ['required','string','max:150','unique:house_types,name,'.$houseType->id]
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $input = $request->only(['name']);
        $input = array_filter($input, 'strlen');
        $houseType->fill($input)->save();
        return redirect()->route('admin.housetypes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HouseType  $houseType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $houseType = HouseType::findOrFail($id);
        $houseType->delete();
        return redirect()->route('admin.housetypes.index');
    }
}
