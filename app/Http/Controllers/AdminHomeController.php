<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\House;
use Carbon\Carbon;
use Lava;

class AdminHomeController extends Controller
{
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('auth:admin');
    // }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::with(['user','house'])->get();
        $houses = House::all();
        $monthly_averages = $payments->groupBy(function($d) {
                                return Carbon::parse($d->created_at)->format('Y-M');
                            })->map(function ($item, $key) {
                                return [
                                    'average' => $item->avg('amount'),
                                    'total' => $item->sum('amount')
                                ];
                            });
        $house_averages = $payments->groupBy(function($d) {
                                return $d->house_id;
                            })->map(function ($item, $key) use ($houses) {
                                return [
                                    "house" => $houses->where('id',$key)->first()->name,
                                    "values" => $item->groupBy(function($d) {
                                            return Carbon::parse($d->created_at)->format('Y-M');
                                        })->map(function ($item, $key) {
                                            return [
                                                'average' => $item->avg('amount'),
                                                'total' => $item->sum('amount')
                                            ];
                                        })->toArray()
                                ];
                            });
        return view('admin.home')->with('monthly_averages',$monthly_averages)->with('house_averages',$house_averages);
    }
}
