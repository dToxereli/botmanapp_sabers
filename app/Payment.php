<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUuid;

class Payment extends Model
{
    use HasUuid;
    public $incrementing = false;

    protected $fillable = ['house_id'.'user_id','amount','mode','details'];

    public function getStateAttribute()
    {
        if ($this->reversed) {
            return "Reversed";
        } else if($this->archived) {
            return "Archived";
        } else if($this->confirmed) {
            return "Confirmed";
        } else {
            return "Not Confirmed";
        }
    }

    public function house() {
        return $this->belongsTo('App\House','house_id');
    }

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function setDetailsAttribute($details)
    {
        $this->attributes['details'] = json_encode($details);
    }

    public function getDetailsAttribute()
    {
        return json_decode($this->attributes['details']);
    }
}
