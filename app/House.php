<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUuid;

class House extends Model
{
    use HasUuid;
    public $incrementing = false;

    protected $fillable = ['name','cost','monthly_plan','house_type_id','house_location_id'];

    public function getTypeAttribute()
    {
        return isset($this->house_type->name) ? $this->house_type->name : "Not set";
    }

    public function getLocationAttribute()
    {
        return isset($this->house_location->name) ? $this->house_location->name : "Not set";
    }
    
    public function getAvailableAttribute()
    {
        return isset($this->user) ? "Taken" : "Available";
    }

    public function getPaidAttribute()
    {
        if (isset($this->attributes['user_id'])) {
            return $this->payments->where('user_id',$this->attributes['user_id'])->where('confirmed',true)->where('reversed',false)->sum('amount');
        } else {
            return 0.00;
        }
    }

    public function getPendingAttribute()
    {
        return $this->cost - $this->paid;
    }

    public function house_type() {
        return $this->belongsTo('App\HouseType','house_type_id');
    }

    public function house_location() {
        return $this->belongsTo('App\HouseLocation','house_location_id');
    }

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function payments() {
        return $this->hasMany('App\Payment','house_id');
    }
}
