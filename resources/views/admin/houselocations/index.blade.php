@extends('layouts.admin')

@section('content')
    <div>
        <h3>Manage house locations</h3>
    </div>

    <div>
        <table class="table table-bordered datatable">
            <thead class="thead-inverse|thead-default">
                <tr>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($houselocations as $houselocation)
                    <tr>
                        <td>{{$houselocation->name}}</td>
                        <td>
                            <div class="row">
                                <a class="btn bg-transparent" href="{{route('admin.houselocations.edit',$houselocation->id)}}" title="Edit">
                                    <span class="mdi mdi-pencil mdi-24px"></span>
                                </a>
                                <form action="{{route('admin.houselocations.destroy',$houselocation->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn bg-transparent text-danger" type="submit" title="Delete">
                                        <span class="mdi mdi-delete mdi-24px"></span>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div>
        <a class="btn btn-primary" href="{{route('admin.houselocations.create')}}" title="Edit">
            <span class="mdi mdi-plus mdi-18px"></span> Add house location
        </a>
    </div>
@endsection

@section('footer')
    @include('includes.datatables')
@endsection