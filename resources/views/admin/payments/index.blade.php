@extends('layouts.admin')

@section('content')
    <div>
        <h3>Manage payments</h3>
    </div>
    <div>
        <table class="table table-bordered table-hover datatable">
            <thead class="thead-inverse|thead-default">
                <tr>
                    <th>House</th>
                    <th>User</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>State</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($payments as $payment)
                        <tr>
                            <td>
                                <a href="{{route('admin.houses.show',$payment->house->id)}}">{{$payment->house->name}}</a>
                            </td>
                            <td>
                                <a href="{{route('admin.owners.show',$payment->user->id)}}">{{$payment->user->name}}</a>
                            </td>
                            <td>{{$payment->amount}}</td>
                            <td>{{$payment->created_at}}</td>
                            <td>{{$payment->state}}</td>
                            <td>
                                <a class="btn text-info bg-transparent" href="{{route('admin.payments.show',$payment->id)}}">
                                    <span class="mdi mdi-more mdi-24px"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
        </table>
    </div>
@endsection

@section('footer')
    @include('includes.datatables')
@endsection