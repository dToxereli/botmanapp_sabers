@extends('layouts.admin')

@section('content')
    <div>
        <h3>Manage house types</h3>
    </div>

    <div>
        <table class="table table-bordered datatable">
            <thead class="thead-inverse|thead-default">
                <tr>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($housetypes as $housetype)
                    <tr>
                        <td>{{$housetype->name}}</td>
                        <td>
                            <div class="row">
                                <a class="btn bg-transparent" href="{{route('admin.housetypes.edit',$housetype->id)}}" title="Edit">
                                    <span class="mdi mdi-pencil mdi-24px"></span>
                                </a>
                                <form action="{{route('admin.housetypes.destroy',$housetype->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn bg-transparent text-danger" type="submit" title="Delete">
                                        <span class="mdi mdi-delete mdi-24px"></span>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div>
        <a class="btn btn-primary" href="{{route('admin.housetypes.create')}}" title="Edit">
            <span class="mdi mdi-plus mdi-18px"></span> Add house type
        </a>
    </div>
@endsection

@section('footer')
    @include('includes.datatables')
@endsection