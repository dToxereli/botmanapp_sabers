@extends('layouts.admin')

@section('content')
    <div>
        <h3>Manage houses</h3>
    </div>

    <div>
        <table class="table table-bordered datatable">
            <thead class="thead-inverse|thead-default">
                <tr>
                    <th>Name</th>
                    <th>Cost</th>
                    <th>Pending</th>
                    <th>Location</th>
                    <th>Type</th>
                    <th>Availability</th>
                    <th>Owner</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($houses as $house)
                    <tr>
                        <td>
                            <a href="{{route('admin.houses.show',$house->id)}}" title="Click to view details">
                                {{$house->name}}
                            </a>
                        </td>
                        <td>Ksh. {{$house->cost}}</td>
                        <td>Ksh. {{$house->pending}}</td>
                        <td>{{$house->location}}</td>
                        <td>{{$house->type}}</td>
                        <td>{{$house->available}}</td>
                        <td>
                            @isset($house->user)
                                <a href="{{route('admin.owners.show',$house->user->id)}}">
                                    {{$house->user->name}}
                                </a>
                            @else
                                Not set
                            @endisset
                        </td>
                        <td>
                            <div class="row">
                                <a class="btn bg-transparent" href="{{route('admin.houses.edit',$house->id)}}" title="Edit">
                                    <span class="mdi mdi-pencil mdi-24px"></span>
                                </a>
                                <form action="{{route('admin.houses.destroy',$house->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn bg-transparent text-danger" type="submit" title="Delete">
                                        <span class="mdi mdi-delete mdi-24px"></span>
                                    </button>
                                </form>
                                <a class="btn bg-transparent" href="{{route('admin.houses.edituser',$house->id)}}" title="Edit">
                                    <span class="mdi mdi-account mdi-24px"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div>
        <a class="btn btn-primary" href="{{route('admin.houses.create')}}" title="Edit">
            <span class="mdi mdi-plus mdi-18px"></span> Add house
        </a>
    </div>
@endsection

@section('footer')
    @include('includes.datatables')
@endsection