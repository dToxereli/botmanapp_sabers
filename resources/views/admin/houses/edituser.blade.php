@extends('layouts.admin')

@section('content')
    <div class="text-center">
        <h3>Edit {{$house->name}} owner</h3>
    </div>

    <div>
        <form action="{{route('admin.houses.updateuser',$house->id)}}" method="POST">
            @csrf
            @method('PUT')

            <div class="form-group row">
                <label for="monthly_plan" class="col-md-4 col-form-label text-md-right">{{ __('Enter Monthly plan [KSH]') }}</label>

                <div class="col-md-6">
                    <input id="monthly_plan" type="number" min="0" step="0.01" class="form-control{{ $errors->has('monthly_plan') ? ' is-invalid' : '' }}" name="monthly_plan" value="{{ old('monthly_plan',$house->monthly_plan) }}" required autofocus>

                    @if ($errors->has('monthly_plan'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('monthly_plan') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            @if ($errors->has('user'))
                <span class="text-danger">
                    <strong>{{ $errors->first('user') }}</strong>
                </span>
            @endif
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email address</th>
                        <th>Phone</th>
                        <th>National id</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="user" id="none" value="" @if (!isset($house->user_id)) checked @endif>
                                    None (Reclaim house)
                                </label>
                            </div>
                        </td>
                        <td>
                            <label for="none">None</label>
                        </td>
                        <td>
                            <label for="none">None</label>
                        </td>
                        <td>
                            <label for="none">None</label>
                        </td>
                    </tr>
                    @foreach ($users as $user)
                        <tr>
                            <td>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="user" id="user-{{$user->id}}" value="{{$user->id}}" @if ($user->id == $house->user_id) checked @endif>
                                        {{$user->name}}
                                    </label>
                                </div>
                            </td>
                            <td>
                                <label for="user-{{$user->id}}">{{$user->email}}</label>
                            </td>
                            <td>
                                <label for="user-{{$user->id}}">{{$user->phone}}</label>
                            </td>
                            <td>
                                <label for="user-{{$user->id}}">{{$user->national_id}}</label>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Update owner') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('footer')
    @include('includes.datatables')
@endsection