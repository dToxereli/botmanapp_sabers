@extends('layouts.admin')

@section('content')
    <div>
        <h3>Manage Enquiries</h3>
    </div>
    <div>
        <table class="table table-bordered datatable">
            <thead class="thead-default">
                <tr>
                    <th>Info</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Date</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($enquiries as $enquiry)
                        <tr>
                            <td>
                                <p><b>E-mail: </b> {{$enquiry->email}}</p>
                                <p><b>Phone: </b> {{$enquiry->phone}}</p>
                                <p><b>Status: </b> {{$enquiry->state}}</p>
                            </td>
                            <td>{{$enquiry->title}}</td>
                            <td>{{$enquiry->message}}</td>
                            <td>{{$enquiry->created_at}}</td>
                            <td>
                                @if (!$enquiry->addressed)
                                    <form action="{{route('admin.enquiries.update',$enquiry->id)}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button class="btn text-primary bg-transparent" type="submit" title="Mark as addressed">
                                            <span class="mdi mdi-check mdi-24px"></span>
                                        </button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
        </table>
    </div>
@endsection

@section('footer')
    @include('includes.datatables')
@endsection