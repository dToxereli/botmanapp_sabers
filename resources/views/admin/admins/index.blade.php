@extends('layouts.admin')

@section('content')
    <div>
        <h3>Manage admin accounts</h3>
    </div>

    <div>
        <table class="table table-bordered datatable">
            <thead class="thead-inverse|thead-default">
                <tr>
                    <th>Name</th>
                    <th>Email-address</th>
                    <th>Phone</th>
                    <th>National ID</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($admins as $admin)
                    <tr>
                        <td>{{$admin->name}}</td>
                        <td>{{$admin->email}}</td>
                        <td>{{$admin->phone}}</td>
                        <td>{{$admin->national_id}}</td>
                        <td>
                            <div class="row">
                                <a class="btn bg-transparent" href="{{route('admin.admins.edit',$admin->id)}}" title="Edit">
                                    <span class="mdi mdi-pencil mdi-24px"></span>
                                </a>
                                <form action="{{route('admin.admins.destroy',$admin->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn bg-transparent text-danger" type="submit" title="Delete">
                                        <span class="mdi mdi-delete mdi-24px"></span>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div>
        <a class="btn btn-primary" href="{{route('admin.admins.create')}}" title="Edit">
            <span class="mdi mdi-plus mdi-18px"></span> Add admin
        </a>
    </div>
@endsection

@section('footer')
    @include('includes.datatables')
@endsection