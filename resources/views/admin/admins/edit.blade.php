@extends('layouts.admin')

@section('content')
    <div class="text-center">
        <h3>Edit {{$admin->name}}</h3>
        <p>
            Fields marked by (*) are mandatory
        </p>
        @if ($errors->count() > 0)
            <p class="text-danger">
                It seems there were errors in your input
            </p>  
        @endif
    </div>

    <div>
        <form action="{{route('admin.admins.update', $admin->id)}}" method="POST" aria-label="{{ __('Add admin') }}">
            @csrf
            @method('PUT')

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $admin->name) }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email',$admin->email) }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                <div class="col-md-6">
                    <input id="phone" type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone', $admin->phone) }}" required>

                    @if ($errors->has('phone'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="national_id" class="col-md-4 col-form-label text-md-right">{{ __('National ID Number') }}</label>

                <div class="col-md-6">
                    <input id="national_id" type="text" class="form-control{{ $errors->has('national_id') ? ' is-invalid' : '' }}" name="national_id" value="{{ old('national_id', $admin->national_id) }}" required>

                    @if ($errors->has('national_id'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('national_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Update details') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection