@extends('layouts.admin')

@section('content')
    <div>
        <h3>Manage owner accounts</h3>
    </div>

    <div>
        <table class="table table-bordered datatable">
            <thead class="thead-inverse|thead-default">
                <tr>
                    <th>Name</th>
                    <th>Email-address</th>
                    <th>Phone</th>
                    <th>National ID</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($owners as $owner)
                    <tr>
                        <td>{{$owner->name}}</td>
                        <td>{{$owner->email}}</td>
                        <td>{{$owner->phone}}</td>
                        <td>{{$owner->national_id}}</td>
                        <td>
                            <div class="row">
                                <a class="btn bg-transparent" href="{{route('admin.owners.show',$owner->id)}}" title="Details">
                                    <span class="mdi mdi-account-card-details mdi-24px"></span>
                                </a>
                                <form action="{{route('admin.owners.destroy',$owner->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn bg-transparent text-danger" type="submit" title="Delete">
                                        <span class="mdi mdi-delete mdi-24px"></span>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('footer')
    @include('includes.datatables')
@endsection