<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SABERS</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        body {
            font-family: 'Raleway', sans-serif;
            margin: 0;
            padding: 0;
            background: radial-gradient(#111, #222);
        }

        .container {
            display: flex;
            height: 100vh;
            align-items: center;
            justify-content: center;
        }

        .content {
            text-align: center;
        }

        .links a {
            font-size: 1.25rem;
            text-decoration: none;
            color: white;
            margin: 10px;
        }

        .links > a {
            color: #ddd;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .links > a:hover, .title:hover {
            color: rgb(214, 84, 222);
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        @media all and (max-width: 500px) {

            .links {
                display: flex;
                flex-direction: column;
            }
        }
    </style>
</head>
<body>
<div class="container">
    <div class="links top-right">
        @auth('web')
            <a href="{{ route('home') }}">Home</a>
        @endauth
            
        @auth('admin')
            <a href="{{ route('admin.home') }}">Home</a>
        @endauth

        @guest('web')
            <a href="{{ route('login') }}">Login</a>
            <a href="{{ route('register') }}">Register</a>
        @endguest

        @guest('admin')
            <a href="{{ route('admin.login') }}">Admin</a>    
        @endguest
    </div>
    <div class="content">
        <h1 style="color: #fff; font-size: 3em;">SABERS</h1>
        <div>
            <img src="{{asset('images/logo.png')}}" class="img-fluid" alt="Morgage payment">
        </div>

        <div class="links">
            <a href="/botman/tinker">Tinker</a>
        </div>
    </div>
</div>
</body>
</html>