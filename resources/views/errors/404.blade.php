@extends('layouts.master')

@section('content')
    <div class="text-center">
        <h1>404 Page not found</h1>
        <h4>Looks like the page you were looking for could not be found</h4>
    </div>
@endsection