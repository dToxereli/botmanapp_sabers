@extends('layouts.master')

@section('content')
    <style>
        input[name="mode"]:checked ~ .payment-details {
            display:block;
        }
        input[name="mode"] ~ .payment-details {
            display:none;
        }
    </style>
    <div class="text-center">
        @if ($errors->count() > 0)
            <ul class="list-group">
                @foreach ($errors->all() as $error)
                    <li class="list-group-item">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </div>
    <div class="row col-8 offset-2">
        <div class="col-md-12">
            <h4 class="mb-3">Billing address</h4>
            <form action="{{route('pay.complete')}}" method="POST" class="needs-validation" >
                @csrf
                <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="house">House number</label>
                    <input type="text" class="form-control" id="house" name="house" placeholder="House number" required>
                    @if ($errors->has('house'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('house') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-6 mb-3">
                    <label for="amount">Amount to pay [KSH]</label>
                    <input type="number" min="0" step="0.01" class="form-control" id="amount" name="amount" placeholder="Amount" required>
                    @if ($errors->has('amount'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('amount') }}</strong>
                        </span>
                    @endif
                </div>
                </div>

                <div class="mb-3">
                    <label for="email">Email <span class="text-muted">(Optional)</span></label>
                    <input type="email" class="form-control" id="email" placeholder="you@example.com">
                </div>

                <hr class="mb-4">

                <h4 class="mb-3">Payment</h4>

                <div class="d-block my-3">
                    <div class="custom-control custom-radio">
                        <input id="card" name="mode" type="radio" class="custom-control-input" value="Card" required data-toggles="#paypal-details">
                        <label class="custom-control-label" for="card">Card</label>
                        <div id="card-details" class="payment-details">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="cc-name">Name on card</label>
                                    <input type="text" class="form-control" id="cc-name" name="cc-name" placeholder="">
                                    <small class="text-muted">Full name as displayed on card</small>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="cc-number">Credit card number</label>
                                    <input type="text" class="form-control" id="cc-number" name="cc-number" placeholder="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <label for="cc-expiration">Expiration</label>
                                    <input type="text" class="form-control" id="cc-expiration" name="cc-expiration" placeholder="">
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="cc-expiration">CVV (3 digit code at the back of your card)</label>
                                    <input type="text" class="form-control" id="cc-cvv" name="cc-cvv" placeholder="">
                                    @if ($errors->has('paypal-email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('paypal-email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="custom-control custom-radio">
                        <input id="paypal" name="mode" type="radio" class="custom-control-input" value="Paypal" required>
                        <label class="custom-control-label" for="paypal">Paypal</label>
                        <div id="paypal-details" class="payment-details">
                            <div class="mb-3">
                                <label for="paypal-email">Paypal account Email</label>
                                <input type="email" class="form-control" id="paypal-email" name="paypal-email" placeholder="you@example.com">
                                @if ($errors->has('paypal-email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('paypal-email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="custom-control custom-radio">
                        <input id="mpesa" name="mode" type="radio" class="custom-control-input" value="Mpesa" required>
                        <label class="custom-control-label" for="mpesa">MPesa</label>
                        <div id="mpesa-phone-details" class="payment-details">
                            <div class="mb-3">
                                <label for="phone">Phone</label>
                                <input type="tel" class="form-control" id="mpesa-phone" name="mpesa-phone" placeholder="+254700000000">
                                @if ($errors->has('mpesa-phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mpesa-phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="custom-control custom-radio">
                        <input id="airtel" name="mode" type="radio" class="custom-control-input" value="Airtel money" required>
                        <label class="custom-control-label" for="airtel">Airtel money</label>
                        <div id="airtel-phone-details" class="payment-details">
                            <div class="mb-3">
                                <label for="phone">Phone</label>
                                <input type="tel" class="form-control" id="airtel-phone" name="airtel-phone" placeholder="+254700000000">
                                @if ($errors->has('airtel-phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('airtel-phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if ($errors->has('method'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('method') }}</strong>
                        </span>
                    @endif
                </div>
                
                <hr class="mb-4">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Make payment</button>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $(function () {  
            $('input[type=radio][name=bedStatus]').change(function() {
                if (this.value == 'allot') {
                    alert("Allot Thai Gayo Bhai");
                }
                else if (this.value == 'transfer') {
                    alert("Transfer Thai Gayo");
                }
            });
        });
    </script>
@endsection