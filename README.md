# Sabers Botman

A laravel + botman based application made for the E-Connected 2018 Hackathon

## Requirements

The application runs on laravel 5.6 therefore will require php 7.1 +. Composer is also required to install dependencies along with npm v5 + and node v8 +.

Additional requirements for the laravel framework can be found [here](https://laravel.com/docs/5.8#server-requirements)

At least one of the following databases is required for the program:

* MySql
* MariaDB
* Postgresql
* Sqlite

## Installation

Unpack the zip file or pull it from the git repository

Launch the terminal and navigate to the project's root directory

Run the following commands:

```bash
composer install
npm install
cp .env.example .env
php artisan storage:link
php artisan key:generate
```

Edit the .env file and setup environment variables especially for the database, site url and deployment environment

If you're running the application using apache, lighthttpd or nginx remember to set appropriate permissions for the servers to be able to modify storage and cache directories

```bash
sudo chgrp -R www-data storage bootstrap/cache
sudo chmod -R ug+rwx storage bootstrap/cache
```

Once that is done, run migrations and serve the application

```bash
php artisan migrate
```

You can serve the application directly with nginx, apache or lighthttpd (or any other server that supports php like iis) or using the built in development server.

```bash
php artisan serve
```
