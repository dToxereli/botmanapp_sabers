<?php

use Illuminate\Database\Seeder;
use App\Admin;
use Illuminate\Support\Facades\DB;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'name' => "System Admin",
            'email' => "systemadmin@example.com",
            'phone' => "+254706514389",
            'national_id' => "+254706514389",
            'password' => "00000000",
        ]);
    }
}
