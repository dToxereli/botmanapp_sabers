<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->uuid('house_id')->nullable();
            $table->uuid('user_id')->nullable();
            $table->decimal('amount',12,2)->default(0);
            $table->boolean('archived')->default(false);
            $table->boolean('reversed')->default(false);
            $table->boolean('confirmed')->default(false);
            $table->string('mode')->nullable();
            $table->text('details');
            $table->primary('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
