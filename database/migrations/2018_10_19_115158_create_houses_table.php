<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->string('name')->unique();
            $table->decimal('cost',12,2)->default(0);
            $table->decimal('monthly_plan',12,2)->default(0);
            $table->uuid('user_id')->nullable();
            $table->uuid('house_type_id')->nullable();
            $table->uuid('house_location_id')->nullable();
            $table->primary('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
